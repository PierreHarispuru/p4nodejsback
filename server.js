const express = require('express');
const app = express();
const PORT = 8081;

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
    });

app.use( express.json() )

app.listen(
    PORT,
    () => console.log(`port:${PORT}`)
)

app.get('/hello', (req,res) => {
    res.status(200).send({
        message: 'hello from back node ?'
    })
});