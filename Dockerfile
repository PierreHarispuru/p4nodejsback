FROM node:alpine

COPY package.json /app/package.json

WORKDIR /app

RUN npm install

EXPOSE 8081

COPY . .

CMD ["node", "server.js"]
